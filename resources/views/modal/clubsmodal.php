<div id="formModal" class="modal fade" role="dialog">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add club</h4>
        </div>
        <div class="modal-body">
         <span id="form_result"></span>
         <form method="post" id="sample_form" class="form-horizontal" enctype="multipart/form-data">
          <!-- @csrf -->
          <div class="form-group">
            <label class="control-label col-md-4" >CLB : </label>
            <div class="col-md-8">
             <input type="text" name="club" id="club" class="form-control" />
            </div>
           </div>
           <div class="form-group">
            <label class="control-label col-md-4">HLV : </label>
            <div class="col-md-8">
             <input type="text" name="coach" id="coach" class="form-control" />
            </div>
            </div>
            <div class="form-group">
            <label class="control-label col-md-4">SVĐ : </label>
            <div class="col-md-8">
             <input type="text" name="stadium" id="stadium" class="form-control" />
            </div>
            </div>
           <div class="form-group">
            <label class="control-label col-md-4">Chọn Logo : </label>
            <div class="col-md-8">
             <input type="file" name="image" id="image" />
             <span id="store_image"></span>
            </div>
           </div>
           <br />
           <div class="form-group" align="center">
            <input type="hidden" name="action" id="action" />
            <input type="hidden" name="hidden_id" id="hidden_id" />
            <input type="submit" name="action_button" id="action_button" class="btn btn-warning" value="Add" />
           </div>
         </form>
        </div>
     </div>
    </div>
</div>