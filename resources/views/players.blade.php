<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Player</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>  
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
    <div class="container">    
        <br />
        <h3 align="center">Player</h3>
        <br />
        <br />
        <div align="right">
            <button type="button" name="create_record" id="create_record" class="btn btn-success btn-sm">Add Player</button>
            <input type="text" class="hidden url" value="{{ route('players.store') }}">

        </div>
        <div class="table-responsive">
            <table class="table table-bordered table-striped" id="user_table">
                <thead>
                    <tr text-align="center">
                        <th>Ten</th>
                        <th>Tuoi</th>
                        <th>Vi tri</th>
                    </tr>
                </thead>
                <tbody>
                    @if($products->isNotEmpty())
                    @foreach($products as $players)
                    <tr>
                        <td>{{ $players->name }}</td>
                        <td>{{ $players->age }}</td>
                        <td>{{ $players->position }}</td>
                        <td class="hidden">{{ $players->club_id }}</td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="3">not found</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
        <br />
        <br />
    </div>
    <div id="formModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Player</h4>
                </div>
                <div class="modal-body">
                    <span id="form_result"></span>
                    <form method="post" id="sample_form" class="form-horizontal" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group hidden">
                            <div class="col-md-8">
                                <input type="text" name="club_id" id="club_id" class="form-control" value="{{ $players->club_id }}" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" >Ten : </label>
                            <div class="col-md-8">
                             <input type="text" name="name" id="name" class="form-control" />
                         </div>
                     </div>
                     <div class="form-group">
                        <label class="control-label col-md-4">Tuoi : </label>
                        <div class="col-md-8">
                            <input type="text" name="age" id="age" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-4">Vi tri : </label>
                        <div class="col-md-8">
                            <input type="text" name="position" id="position" class="form-control" />
                        </div>
                    </div>
                    <br />
                    <div class="form-group" align="center">
                        <input type="hidden" name="action" id="action" />
                        <input type="hidden" name="hidden_id" id="hidden_id" />
                        <input type="submit" name="action_button" id="action_button" class="btn btn-warning" value="Add" />
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
<script src="{{url('js/players.js')}}"></script>
</html>
