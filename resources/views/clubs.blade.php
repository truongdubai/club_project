<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Premiere League</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>  
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
  <nav class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
      <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav navbar-right">
          <li class="dropdown">
            <a href="{{url('login')}}" class="dropdown-toggle" data-toggle="dropdown">{{Auth::user()->name}} <b class="caret"></b></a>
            <ul class="dropdown-menu">
              <li><a href="{{url('logout')}}">Logout</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="container">    
    <br />
    <h3 align="center">CLUB</h3>
    <br />
    <div align="right">
      <button type="button" name="create_record" id="create_record" class="btn btn-success btn-sm">Add Club</button>
    </div>
    <br />
    <div class="table-responsive">
      <table class="table table-bordered table-striped" id="user_table">
       <thead>
        <tr>
          <th width="10%">Logo</th>
          <th width="30%">Club</th>
          <th width="15%">HLV</th>
          <th width="15%">SVD</th>
          <th width="30%">Action</th>
        </tr>
      </thead>
      <tr>
       
      </tr>
    </table>
  </div>
  <br />
  <br />
</div>
@include('modal.clubsmodal')
@include('modal.deletemodal')
</body>
<script src="{{url('js/clubs.js')}}"></script>
</html>