$(document).ready(function(){
  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $('#create_record').click(function(){
    $('.modal-title').text("Them CLB");
      $('#action_button').val("Add");
      $('#action').val("Add");
      $('#formModal').modal('show');
  });

  $('#sample_form').on('submit', function(event){
    event.preventDefault();
    if($('#action').val() == 'Add')
    {
      var url = $('.url').val();
      $.ajax({
      url: url,
      method:"POST",
      data: new FormData(this),
      contentType: false,
      cache:false,
      processData: false,
      dataType:"json",
      success:function(data)
      {
        console.log(data);
        var html = '';
        if(data.errors)
        {
          html = '<div class="alert alert-danger">';
          for(var count = 0; count < data.errors.length; count++)
        {
          html += '<p>' + data.errors[count] + '</p>';
        }
          html += '</div>';
        }
        if(data.success)
        {
          html = '<div class="alert alert-success">' + data.success + '</div>';
        }
        $('#form_result').html(html);
        $('.close').click();
        window.location.reload();
        $('#formModal').modal('hide');
      }
    })
    }
  });
});