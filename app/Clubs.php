<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\Clubs as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Requests\StoreRequest;
use Illuminate\Http\Requests\EditRequest;
use Validator;
class Clubs extends Model
{
	protected $table = 'clubs';
    protected $fillable = ['club', 'coach', 'stadium', 'image'];
    public function players()
    {
        return $this->hasMany('App\Players', 'club_id', 'id');
    }

    public static function index()
    {
        if(request()->ajax())
        {
            return datatables()->of(Clubs::latest()->get())
                    ->addColumn('action', function($data){
                        $button = '<button type="button" name="edit" id="'.$data->id.'" class="edit btn btn-primary btn-sm">Edit</button>';
                        $button .= '&nbsp;&nbsp;';
                        $button .= '<button type="button" name="delete" id="'.$data->id.'" class="delete btn btn-danger btn-sm">Delete</button>';
                        $button .= '&nbsp;&nbsp;';
                        $button .= '<a href="/players/'.$data->id.'" name="show" id="'.$data->id.'" class="btn btn-success btn-sm">Show</a>';
                        return $button;

                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('clubs');
    }
    public static function store($request)
    {
        $image = $request->file('image');

        $new_name = rand() . '.' . $image->getClientOriginalExtension();

        $image->move(public_path('images'), $new_name);

        $form_data = array(
            'club'        =>  $request->club,
            'coach'       =>  $request->coach,
            'stadium'     =>  $request->stadium,
            'image'       =>  $new_name
        );

        Clubs::create($form_data);

        return response()->json(['success' => 'Them thanh cong.']);
    }
    public static function edit($id)
    {
        if(request()->ajax())
        {
            $data = Clubs::findOrFail($id);
            return response()->json(['data' => $data]);
        }
    }
    public static function updatenew ($request)
    {
        $image_name = $request->hidden_image;
        $image = $request->file('image');
        if($image != '')
        {
            $image_name = rand() . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('images'), $image_name);
        }
        $form_data = array(
            'club'       =>   $request->club,
            'coach'      =>   $request->coach,
            'stadium'    =>   $request->stadium,
            'image'      =>   $image_name
        );
        Clubs::whereId($request->hidden_id)->update($form_data);

        return response()->json(['success' => 'Data is successfully updated']);
    }
    public static function destroy($id)
	{
	   $data = Clubs::findOrFail($id);
	   $data->players()->delete();
       $data->delete();
	}
}
