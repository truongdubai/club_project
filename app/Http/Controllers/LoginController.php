<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use Validator;
use Auth;
use Illuminate\Support\MessageBag;

class LoginController extends Controller
{
    
    public function getLogin() {
    	return view('login');
    }
    public function postLogin(Request $request) {
    	return User::postLogin($request);
    }
}