<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class LogoutController extends Controller
{
    public function __construct() {
    	$this->middleware('auth',['except' => 'getLogout']);
    }

    public function getIndex() {
    	return redirect()->intended('/');
    }
    public function getLogout() {
   		Auth::logout();
   		return redirect(\URL::previous());
	}
}