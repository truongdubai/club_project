<?php

namespace App\Http\Controllers;
use App\Clubs;
use App\Http\Requests\StoreRequest;
use App\Http\Requests\EditRequest;
use Validator;
use Illuminate\Support\Facedes\Auth;
use Illuminate\Support\MessageBag;

class ClubsController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return Clubs::index();

    }
    public function create()
    {
        //
    }
    public function store(StoreRequest $request)
    {
        return Clubs::store($request);
    }
    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        return Clubs::edit($id);
    }
    public function update(EditRequest $request)
    {
        return Clubs::updatenew($request);
    }
    public function destroy($id)
    {
        return Clubs::destroy($id);
    }
}