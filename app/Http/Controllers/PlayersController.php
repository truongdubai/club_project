<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Players;
use Validator;
use Auth;
use Illuminate\Support\MessageBag;
use App\Http\Requests\StorePlayersRequest;

class PlayersController extends Controller
{
	public function index()
    {
    	$data['products'] = Players::all();
        return view('players', $data);
    }
    public function show($id){
    	$data['products']=Players::showInfomation($id);
    	return view('players',$data);
    }
    public function store(StorePlayersRequest $request)
    {
        return Players::store($request);
    }
}