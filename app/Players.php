<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\Players as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Requests\StorePlayersRequest;
use Validator;
use Auth;

class Players extends Model
{
    protected $table = 'players';
    protected $fillable = ['club_id','name', 'age', 'position'];
    protected $hidden = ['created_at', 'updated_at'];

    public static function clubs()
    {
        return $this->belongsTo('App\Clubs', 'club_id', 'name');
    }
    public static function showInfomation($id){
    	$showInfo = Players::where('club_id',$id)->get();
    	return $showInfo;
    }
    public static function store($request)
    {
        $form_data = array(
        	'club_id'	  =>  $request->club_id,
            'name'        =>  $request->name,
            'age'         =>  $request->age,
            'position'    =>  $request->position
        );

        Players::create($form_data);

        return response()->json(['success' => 'Them thanh cong.']);
    }
}